# GifEncoder #

This is a port of ofxGifEncoder(and soon ofxGifDecoder) by [Jesus Gollonet](https://github.com/jesusgollonet/ofxGifEncoder) for use with [Cinder](http://libcinder.org)

### What is this repository for? ###

* to encode gifs!
* some added functionality - can accept a vector of ci::Surface8uRefs as well as specify a callback function to call once encoding is done

### Setup ###

* mostly header only so should be pretty simple to include in terms of files
* [FreeImage](http://freeimage.sourceforge.net/) is a dependency. OSX static lib and file are included. So far only tested on OSX
* files are in the `include` and `src` folders. To launch the included sample project, the folder needs to be in your `Documents` folder under a subfolder called `projects`. Your Cinder installation needs to be in `Documents` as well with the folder name of `Cinder`. These settings can be changed in the Xcode project file.


### To Dos ###

* need to add threaded functionality
* need to integrate decoder functionality as well.