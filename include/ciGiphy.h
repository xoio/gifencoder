//
//  ciGiphy.h
//  Port of ofxGifEncoder and ofxGifDecoder by Jesus Gollonet
//  https://github.com/jesusgollonet/ofxGifEncoder
//  https://github.com/jesusgollonet/ofxGifDecoder
//
//

#ifndef ciGiphy_h
#define ciGiphy_h

#include "GifEncoder.hpp"
#include "GifDecoder.hpp"

#endif /* ciGiphy_h */
