//
//  GifEncoder.hpp
//  GifEncoder
//  Port of ofxGifEncoder by Jesus Gollonet
//  https://github.com/jesusgollonet/ofxGifEncoder
//  Requires FreeImage as a dependency.
//
//  TODO setup threading so it can (hopefully) save in near-realtime
//  TODO work on decoding for playback

#ifndef GifEncoder_hpp
#define GifEncoder_hpp

#include "FreeImage.h"
#include "ditherTypes.h"
#include "GifFrame.hpp"
#include "cinder/Log.h"
#include "cinder/Signals.h"

#include "cinder/ConcurrentCircularBuffer.h"
#include "cinder/Thread.h"

class GifEncoder{
    
    //! Concurrent buffer for queueing and handling loading in a orderly fashion
    ci::ConcurrentCircularBuffer<ci::Surface8uRef>	* mBuffer;
    
    //! an function that gets run once gif encoding is complete
    std::function<void ()> onComplete;
    
    //! signals object to hold callbacks that don't require a value passed back.
    ci::signals::Signal<void (void)> vsignal;
    
    //! Thread object for helping to load all of the images
    std::shared_ptr<std::thread> mThread;
    
    bool isRecording;
    bool mCanceled;
    
    std::string savePath;
    
    float recordingStartTime;
    
    void startEncoding(ci::gl::ContextRef context);
public:
    
    GifEncoder();
    ~GifEncoder();
    
    void setup(int _w, int _h, float _frameDuration = 0.1f, int _nColors = 256 );
    void setNumColors(int _nColors = 256);
    void setDitherMode(int _ditherMode = OFX_GIF_DITHER_FS);
    void setFrameDuration(float _duration); // for all gifs;
    
    void setFilename(std::string fileName);
    
    //! connect function to link a function to the listener without needing a returned value.
    template <typename A>
    void onEncodeComplete(void (A::* callbackFunction)(), A * callbackObject){
        auto b = std::bind(callbackFunction, callbackObject);
        vsignal.connect(b);
    }
    
    void startRecording(std::string savePath="");
    void stopRecording();
    
    // thread saving
    // blocking, verbose
    
    void exit();
    void reset();
    
    void stopAndJoin();
    
    // if no duration is specified, we'll use default (from setup())
    void addFrame(unsigned char * px, int _w, int _h, int _bitsPerPixel = 24, float duration = 0.f);
    
    //! adds a set of images
    void addFrames(std::vector<ci::Surface8uRef> imgs,int _bitsPerPixel=24,float duration=0.0f);
    
    static GifFrameRef createGifFrame(unsigned char * px, int _w, int _h, int _bitsPerPixel = 24, float duration = 0.1f);
    void save(std::string _fileName = "test.gif" );

    
    void calculatePalette(FIBITMAP * bmp);
    int getClosestToGreenScreenPaletteColorIndex();
    ci::Color greenScreenColor;
    std::vector <ci::Color> palette;
    GifFrameRef convertTo24BitsWithGreenScreen(GifFrameRef frame);
    void processFrame(GifFrameRef frame, FIMULTIBITMAP * multi);
    void swapRgb(GifFrameRef pix);
    void threadedFunction();
    void doSave();
    bool bSaving;
    
    std::vector <GifFrameRef> frames;
    std::string  fileName;
    int     nColors;
    float   frameDuration;
    int w;
    int h;
    int bitsPerPixel;
    int nChannels;
    int ditherMode;
    
    
};




#endif /* GifEncoder_hpp */
