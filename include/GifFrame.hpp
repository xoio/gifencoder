//
//  GFrame.hpp
//  GifEncoder
//  Ported and re-mangled a bit from work done by Jesus Gollonet as part of
//  ofxGifEncoder and ofxGifDecoder
//  https://github.com/jesusgollonet/ofxGifDecoder
//  Created by Joseph Chow on 3/16/16.
//
//

#ifndef GFrame_hpp
#define GFrame_hpp

#include "cinder/Log.h"
typedef std::shared_ptr<class GifFrame> GifFrameRef;

class GifFrame{
    
public:
    GifFrame();
    GifFrame(unsigned char * px, int _w, int _h, int _bitsPerPixel, float _duration);
    
    static GifFrameRef create(){
        return GifFrameRef(new GifFrame());
    }
    
    static GifFrameRef create(unsigned char * px, int _w, int _h, int _bitsPerPixel, float _duration){
        return GifFrameRef(new GifFrame(px,_w,_h,_bitsPerPixel,_duration));
    }
    
    // eventually localPalette, interlaced, disposal method
    // for user
    void setFromPixels(ci::Surface8uRef img , int _left , int _top, float _duration = 0.f);
    // for ofxGifFile
    void setFromGifPixels(unsigned char * _constructedPx, unsigned char * _rawPx , int _left , int _top, float _duration = 0.f);
    void draw(float _x, float _y);
    void draw(float _x, float _y, int _w, int _h);
    unsigned char * 	getRawPixels();
    int getWidth();
    int getHeight();
    int getLeft();
    int getTop();
    

    int width;
    int height;
    int bitsPerPixel;
    float duration;
    ci::Surface8uRef frameImage;
    unsigned char * pixels;
    void test(){
        cinder::app::console()<<"test";
    }
private:
    int top;
    int left;
   
   
    ci::gl::TextureRef tx;
    // optional
  
    // optional
    std::vector<ci::ColorA> palette;
    
};
#endif /* GFrame_hpp */
