//
//  GifFile.cpp
//  GifEncoder
//  
//  Created by Joseph Chow on 3/25/16.
//
//

#include "GifFile.hpp"
using namespace std;
using namespace ci;
using namespace ci::app;

GifFile::GifFile(){
    w = h = 0;
}

GifFile::~GifFile(){}

void GifFile::setup(int _w, int _h, vector<ci::Color> _globalPalette, int _nPages){
    w               = _w;
    h               = _h;
    globalPalette   = _globalPalette;
    nPages          = _nPages;
}

// by now we're copying everything (no pointers)
void GifFile::addFrame(ci::Surface8uRef _px, int _left, int _top, GifFrameDisposal disposal, float _duration){
    GifFrame f;
    
   
    if(getNumFrames() == 0){
        accumPx = _px; // we assume 1st frame is fully drawn
        f.setFromPixels(_px , _left, _top, _duration);
        gifDuration = _duration;
    } else {
        // add new pixels to accumPx
        int cropOriginX = _left;
        int cropOriginY = _top;
        
        // [todo] make this loop only travel through _px, not accumPx
        for (int i = 0; i < accumPx->getWidth() * accumPx->getHeight(); i++) {
            int x = i % accumPx->getWidth();
            int y = i / accumPx->getWidth();
            
            if (x >= _left  && x < _left + _px->getWidth()  &&
                y >= _top   && y < _top  + _px->getHeight()){
                int cropX = x - cropOriginX;  //   (i - _left) % _px.getWidth();
                int cropY = y - cropOriginY;
                //int cropI = cropX + cropY * _px.getWidth();
                if(_px->getPixel(vec2(cropX,cropY)).a == 0){
                    switch ( disposal ) {
                        case GIF_DISPOSAL_BACKGROUND:
                            _px->setPixel(vec2(x,y), bgColor);
                            break;
                            
                        case GIF_DISPOSAL_LEAVE:
                        case GIF_DISPOSAL_UNSPECIFIED:
                            _px->setPixel(vec2(x,y), accumPx->getPixel(vec2(x,y)));
                            
                            break;
                            
                        case GIF_DISPOSAL_PREVIOUS:
                            _px->setPixel(vec2(x,y), accumPx->getPixel(vec2(cropX,cropY)));
                            break;
                    }
                } else {
                    accumPx->setPixel(vec2(x,y), _px->getPixel(vec2(cropX,cropY)));
                }
            } else {
                if ( _px->getPixel(vec2(x, y)) == bgColor ){
                    switch ( disposal ) {
                        case GIF_DISPOSAL_BACKGROUND:
                            accumPx->setPixel(vec2(x,y), bgColor);
                            break;
                            
                        case GIF_DISPOSAL_UNSPECIFIED:
                        case GIF_DISPOSAL_LEAVE:
                         accumPx->setPixel(vec2(x,y), _px->getPixel(vec2(x,y)));
                            break;
                            
                        case GIF_DISPOSAL_PREVIOUS:
                            _px->setPixel(vec2(x,y), accumPx->getPixel(vec2(x,y)));
                
                            break;
                    }
                } else {
                    accumPx->setPixel(vec2(x,y), _px->getPixel(vec2(x,y)));
                }
            }
        }
        
        f.setFromPixels(_px,_left, _top, _duration);
    }
    accumPx = _px;
    
    //
    gifFrames.push_back(f);
}

int GifFile::getWidth(){
    return w;
}

int GifFile::getHeight(){
    return h;
}

float GifFile::getDuration(){
    return gifDuration;
}

void GifFile::draw(float _x, float _y){
    gifFrames[0].draw(_x, _y);
}

void GifFile::drawFrame(int _frameNum, float _x, float _y){
    if(_frameNum < 0 || _frameNum >= gifFrames.size()){
        CI_LOG_W("GifFile::drawFrame frame out of bounds. not drawing");
        return;
    }
    gifFrames[_frameNum].draw(_x , _y , gifFrames[_frameNum].getWidth(), gifFrames[_frameNum].getHeight());
}

void GifFile::drawFrame(int _frameNum, float _x, float _y, int _w, int _h){
    if(_frameNum < 0 || _frameNum >= gifFrames.size()){
        CI_LOG_W("GifFile::drawFrame frame out of bounds. not drawing");
        return;
    }
    gifFrames[_frameNum].draw(_x , _y , _w, _h);
}


void GifFile::setBackgroundColor(ci::Color _c) {
    bgColor = _c;
    cout<<bgColor<<endl;
}

ci::Color GifFile::getBackgroundColor() {
    return bgColor;
}


int GifFile::getNumFrames() {
    return gifFrames.size();
}

GifFrame * GifFile::getFrameAt(int _index) {
    return &(gifFrames[_index]); //??
}


vector <ci::Color> GifFile::getPalette(){
    return globalPalette;
}

void GifFile::clear() {
    gifFrames.clear();
    globalPalette.clear();
}
